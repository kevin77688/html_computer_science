
image1 = new Image();	image1.src = "1.png";	var p1 = 0;

image2 = new Image();	image2.src = "2.png";	var p2 = 0;
	
image3 = new Image();	image3.src=  "3.png";	var p3 = 0;	

image4 = new Image();	image4.src=  "4.png";	var p4 = 0;

image5 = new Image();	image5.src=  "5.png";	var p5 = 0;

image6 = new Image();	image6.src=  "6.png";	var p6 = 0;

image7 = new Image();	image7.src=  "7.png";	var p7 = 0;

image8 = new Image();	image8.src=  "8.png";	var p8 = 0;

imageblank = new Image();	imageblank.src = "back.jpg";

var order = new Array(16) ;
var cur;
var j = 8 ;
var k = 8 ;


function start()
{
	var table = document.getElementById("table");
	
	for(var i = 1 ; i <= 16 ; i++)
		order[i] = 0;
	
	while (j)
	{
		if(order[cur = Math.floor( 1 + Math.random() * 16 )] == 0)
			order[cur] = j--;
	}
	
	while (k)
	{
		if(order[cur = Math.floor( 1 + Math.random() * 16 )] == 0)
			order[cur] = k--;
	}
	
	for(var a = 1 ; a <= 8 ; a++)
	{
		document.getElementById( "img" + a ).setAttribute ( "id" ,  order[a] );
		document.getElementById( "img" + (a + 8) ).setAttribute ( "id" ,  order[a + 8 ] );
	}
	
}

function refresh()
{
	location.reload();
}

function mouseOver( e )
{  
   
   if ( e.target.getAttribute( "id" ) == "1" )
   {
      e.target.setAttribute( "src", image1.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "2" )
   {
      e.target.setAttribute( "src", image2.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "3" )
   {
      e.target.setAttribute( "src", image3.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "4" )
   {
      e.target.setAttribute( "src", image4.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "5" )
   {
      e.target.setAttribute( "src", image5.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "6" )
   {
      e.target.setAttribute( "src", image6.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "7" )
   {
      e.target.setAttribute( "src", image7.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "8" )
   {
      e.target.setAttribute( "src", image8.getAttribute( "src" ) );
   } 
    
   
}

function mouseOut( e )
{
   if ( e.target.getAttribute( "id" ) == "1" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "2" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "3" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "4" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "5" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "6" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "7" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
   
      if ( e.target.getAttribute( "id" ) == "8" )
   {
      e.target.setAttribute( "src", imageblank.getAttribute( "src" ) );
   } 
}


document.addEventListener( "mouseover", mouseOver, false );
document.addEventListener( "mouseout", mouseOut, false );
window.addEventListener( "load", start, false );

/*************************************************************************
* (C) Copyright 1992-2012 by Deitel & Associates, Inc. and               *
* Pearson Education, Inc. All Rights Reserved.                           *
*                                                                        *
* DISCLAIMER: The authors and publisher of this book have used their     *
* best efforts in preparing the book. These efforts include the          *
* development, research, and testing of the theories and programs        *
* to determine their effectiveness. The authors and publisher make       *
* no warranty of any kind, expressed or implied, with regard to these    *
* programs or to the documentation contained in these books. The authors *
* and publisher shall not be liable in any event for incidental or       *
* consequential damages in connection with, or arising out of, the       *
* furnishing, performance, or use of these programs.                     *
*************************************************************************/